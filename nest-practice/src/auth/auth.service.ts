import { BadRequestException, Injectable } from '@nestjs/common';
import UserService from 'src/user/user.service';
import LoginDto from './DTOs/login.dto';
import { checkEncryptPassword } from 'src/helper/encryptPassword';

@Injectable()
export class AuthService {
    constructor(private userService: UserService) {

    }

    async login(data: LoginDto) {
        try {
            const { email, password } = data
            const [user] = await this.userService.findAll({ email })
            if (!(await checkEncryptPassword(password, user.password))) {
                throw new BadRequestException("Invalid userid or password")
            }
            return user

        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
    }
}
