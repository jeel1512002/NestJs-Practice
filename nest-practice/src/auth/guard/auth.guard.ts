import { CallHandler, CanActivate, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';


export class AuthGuard implements CanActivate {
    canActivate(context: ExecutionContext) {
        const request = context.switchToHttp().getRequest();
        // request.name='jeel'
        return request.session.userId;
      }
}

