import { Body, Controller, Post, Session } from '@nestjs/common';
import LoginDto from './DTOs/login.dto';
import { AuthService } from './auth.service';
import Serialize from 'src/interceptors/serialize.interceptors';
import UserDto from 'src/user/DTOs/user.dto';

@Controller('auth')
@Serialize(UserDto)
export class AuthController {

    constructor(private authService: AuthService) { }

    @Post('/login')
    async login(@Body() body: LoginDto, @Session() session: any) {
        const user = await this.authService.login(body);
        session.userId = user._id;
        return user
    }
    @Post('/logout')
    async logout(@Session() session: any) {
        session.userId = null;
        return "Logged Out successfully"
    }
}
