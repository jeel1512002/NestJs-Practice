import * as bcrypt from "bcrypt"
import * as dotenv from "dotenv"

dotenv.config({ path: '.env' })

export const encryptPassword = async (password: string) => {
    const encPassword = await bcrypt.hash(password, parseInt(process.env.SALT_ROUND))
    return encPassword
}

export const checkEncryptPassword = async (password: string, encPassword: string) => {
    const result = await bcrypt.compare(password, encPassword)
    return result
}