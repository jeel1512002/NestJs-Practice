import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
const cookieSession = require('cookie-session');
// import connectMongo from "./databases/connectMongo"

// (
//   async function () {
//     await connectMongo()
//   }
// )();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(
    cookieSession({
      keys: ['Secerate key'],
    }),
  );
  app.useGlobalPipes(
    new ValidationPipe({ whitelist: true })  // apply validation for all route
  )

  await app.listen(3000);
}
bootstrap();
