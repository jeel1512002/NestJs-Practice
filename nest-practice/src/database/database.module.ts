import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv'
import userSchema from './models/user.schema';
dotenv.config({ path: '.env' })


@Module({
    imports: [MongooseModule.forRoot(process.env.MONGO_URI), MongooseModule.forFeature([{ name: 'user', schema: userSchema }])],
    exports: [MongooseModule]
})
export class DatabaseModule { }
