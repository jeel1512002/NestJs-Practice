import mongoose, { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Gender } from 'src/constants/constants';
import { encryptPassword } from 'src/helper/encryptPassword';

@Schema({
    timestamps: true,
})
export class UserSchema {
    @Prop({ required: true })
    name: string;

    @Prop({ lowercase: true, required: true, unique: true })
    email: string;

    @Prop({ required: true })
    password: string;

    @Prop({ type: String, enum: ['male', 'female'], default: Gender.male })
    gender?: Gender;

    @Prop({
        max: new Date(
            new Date().setFullYear(new Date().getFullYear() - 10),
        ).toLocaleDateString()
    })
    dob?: Date;
}

export type UserDocument = UserSchema & Document

const userSchema = SchemaFactory.createForClass(UserSchema);

userSchema.pre("save", async function (next) {
    this.password = await encryptPassword(this.password);
})

userSchema.pre("findOneAndUpdate", async function (next) {
    if (this["_update"].password) {
        this["_update"].password = await encryptPassword(this["_update"].password)
    }
    next();
})


userSchema.index({ email: 1 })
export default userSchema;
