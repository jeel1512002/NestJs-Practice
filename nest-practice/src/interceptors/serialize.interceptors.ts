import { CallHandler, ExecutionContext, NestInterceptor, UseInterceptors } from "@nestjs/common";
import { plainToInstance } from "class-transformer";
import { Observable, map } from "rxjs";

interface ClassConstructor{         // to check parameter accept class only.
    new (...args: any[]): Object;
}

export default function Serialize(dto: ClassConstructor) {
    return UseInterceptors(new SerializeInterceptor(dto))
}
    

export class SerializeInterceptor implements NestInterceptor {

    constructor(private Dto: ClassConstructor) {

    }

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        // this will run before handler
        return next.handle().pipe(
            map((data: any) => {
                // this will run after handler
                return plainToInstance(this.Dto, data, {
                    excludeExtraneousValues: true
                })
            })
        )
    }
}