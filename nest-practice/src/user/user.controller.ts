import { Body, Controller, Delete, Get, Head, NotFoundException, Param, Post, Put, Query, Request, Session, UseGuards } from '@nestjs/common';
import AddUserDto from './DTOs/addUser.dto';
import UserService from './user.service';
import UpdateUserDto from './DTOs/updateUser.dto';
import UserDto from './DTOs/user.dto';
import Serialize, { SerializeInterceptor } from 'src/interceptors/serialize.interceptors';
import { AuthGuard } from 'src/auth/guard/auth.guard';


// @Query() @Head()

@Controller('user')    // we can give route in either controller decorator or @Get or other request decorator 
@Serialize(UserDto) // To apply on whole route
export class UserController {

    constructor(private userService: UserService) { }
    @Get()
    @UseGuards(AuthGuard)
    async getAllUser(@Query() query: UpdateUserDto, @Request() request: any) {
        return this.userService.findAll(query)
    }

    @UseGuards(AuthGuard)
    @Get("/:id")
    async getUserById(@Param('id') id: string) {
        // console.log(id);
        const data = await this.userService.findOne(id)
        if (!data) {
            throw new NotFoundException('Invalid Id')
        }
        return data
    }

    @Post()
    // @Serialize(UserDto)
    async addUser(@Body() body: AddUserDto, @Session() session: any) {
        const user = await this.userService.addUser(body)
        session.userId = user._id;
        return user
    }

    @UseGuards(AuthGuard)
    @Put("/:id")
    async updateUser(@Param("id") id: string, @Body() body: UpdateUserDto) {
        return this.userService.updateUser(id, body)
    }

    @UseGuards(AuthGuard)
    @Delete("/:id")
    async deleteUser(@Param("id") id: string) {
        const data = await this.userService.deleteUser(id)
        if (!data) {
            throw new NotFoundException('Invalid Id')
        }
        return data
    }
}
