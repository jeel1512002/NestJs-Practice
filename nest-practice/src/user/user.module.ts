import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import UserService from './user.service';
import { DatabaseModule } from 'src/database/database.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  controllers: [UserController],
  providers: [UserService],
  imports: [DatabaseModule],
  exports: [UserService]
  // exports: [UserRepository], //  By using this we can export dependancy for outer module
  // imports: [UserRepository]  //  By using this we can import dependancy from outer module
})

export class UserModule { }
