import AddUserDto from "./DTOs/addUser.dto";
import { BadRequestException, Injectable } from "@nestjs/common";
import UpdateUserDto from "./DTOs/updateUser.dto";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { UserDocument } from "src/database/models/user.schema";

@Injectable()
class UserService {
    constructor(@InjectModel("user") private User: Model<UserDocument>) {

    }
    async findAll(query:Partial<AddUserDto>) {
        try {
            const users = await this.User.find(query);
            return users;
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
        // return this.userRepo.findAll();
    }
    async findOne(id: string) {
        try {
            const user = await this.User.findById(id);
            return user;
        }
        catch (e) {
            throw new BadRequestException("Invalid Id")
        }
        // return this.userRepo.findOne(id);
    }
    async addUser(data: AddUserDto) {
        try {
            const user = await this.User.create(data)
            return user;
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
        // return this.userRepo.addUser(data);
    }
    async updateUser(id: string, data: UpdateUserDto) {
        try {
            const user = await this.User.findByIdAndUpdate(id, data, { new: true, runValidators: true })
            return user;
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
        // return this.userRepo.updateUser(id, data)
    }
    async deleteUser(id: string) {
        try {
            const user = await this.User.findByIdAndRemove(id)
            return user;
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
        // return this.userRepo.deleteUser(id)
    }
}

export default UserService;