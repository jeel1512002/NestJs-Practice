import { Expose } from "class-transformer";
import { Gender } from "src/constants/constants";

class UserDto {
    @Expose()
    name: string;
    
    @Expose()
    email: string

    @Expose()
    dob: Date

    @Expose()
    gender: Gender
}

export default UserDto;