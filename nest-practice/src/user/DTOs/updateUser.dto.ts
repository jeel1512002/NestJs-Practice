import { PartialType } from "@nestjs/mapped-types";
import AddUserDto from "./addUser.dto";

class UpdateUserDto extends PartialType(AddUserDto){

}

export default UpdateUserDto;