import { IsEmail, IsEnum, IsISO8601, IsOptional, IsString, IsStrongPassword, MinLength } from "class-validator"
import { Gender } from "src/constants/constants";

// first class-transformer create AddUserDto class instant (create instance of Dto class) from body data and then class-validator validate data

class AddUserDto {

    @MinLength(2, { message: "Name is too short" })
    @IsString()
    name: string;

    @IsEmail()
    email: string

    @IsStrongPassword()
    @MinLength(5, { message: "Password is too short" })
    password: string

    @IsOptional()
    @IsISO8601({ strict: true })
    dob: Date

    @IsOptional()
    @IsEnum(Gender)
    gender: Gender
}

export default AddUserDto;